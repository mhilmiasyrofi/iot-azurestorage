#ifndef _cmd_ctr_H
#define _cmd_ctr_H

#define MAX_CONNECTION_STRING_LEN (255)

#ifdef __cplusplus
extern "C" {
#endif

///////////////////////////////////////////////////////////////////////////////////////////////////
void command_center_run(void);

#ifdef __cplusplus
}
#endif

#endif//_cmd_ctr_H


#ifndef __SENSOR_H
#define __SENSOR_H

#ifdef __cplusplus
extern "C" {
#endif

void initSensor(void);
void getNextSample(float* HeartBeatRead, float* LatitudeRead, float* LongitudeRead);

#ifdef __cplusplus
}
#endif


#endif//__SENSOR_H


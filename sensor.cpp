#include <Arduino.h>
#include "sensor.h"

uint32_t delayMS;
uint32_t nextSampleAllowedMS = 0;

void initSensor(void) {

  Serial.println("------------------------------------");
  Serial.println("------ Sensor Initialization -------");
  // Set delay between sensor readings based on sensor details.
  delayMS = 1000;
}

void getNextSample(float* HeartBeatRead, float* LatitudeRead, float* LongitudeRead)
{
  // Enforce a delay between measurements.
  uint32_t currTimeMS = millis();
  if (currTimeMS < nextSampleAllowedMS) return;
  nextSampleAllowedMS = currTimeMS + delayMS;
  
  // Get HeartBeatRead event and print its value.
  Serial.print("HeartBeatRead: ");
  int hb =  rand() % 100 + 1;
  Serial.print(hb);
  Serial.println(" *C");
  *HeartBeatRead = hb;

  // Get LatitudeRead event and print its value.
  Serial.print("LatitudeRead: ");
  int lat =  rand() % 10000 + 1;
  float f_lat = lat/100;
  Serial.print(f_lat);
  Serial.println("%");
  *LatitudeRead = f_lat;

  // Get LatitudeRead event and print its value.
  Serial.print("LongitudeRead: ");
  int longitude =  rand() % 10000 + 1;
  float f_long = longitude/100;
  Serial.print(f_long);
  Serial.println("%");
  *LongitudeRead = f_long;

  // biar ga terlalu banyak yang dikirim
  delay(3000);

}


